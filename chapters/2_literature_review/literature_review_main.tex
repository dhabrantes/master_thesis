\chapter{Revisão da Literatura}
\label{ch:literature_review}

\begin{quotation}[]{Confucius}
O silêncio é um amigo verdadeiro que nunca trai.\\
Silence is a true friend who never betrays.
\end{quotation}


% =============================================================================================================================
% =============================================================================================================================
% =============================================================================================================================


\section{Linhas de Produto de Software: Uma visão geral}
\label{sc:linhas_de_produto_de_software}

A \ac{ELPS} tem seus princípios inspirados em fábricas de automóveis, que possuiam produtos com um custo mais interessante quando produzidos em massa do que quando produzidos de forma individual. Estas fábricas utilizam uma plataforma comum para derivar os produtos que podem ser customizados de acordo com as necessidades de um segmento de mercado \citep{Clements2001}. 

Uma linha de produto pode ser definida como um grupo de sistemas de \textit{software} que compartilham um conjunto de características em comum e que satisfazem as necessidades específicas de um segmento de mercado. Este grupo de sistemas são desenvolvidos com base em um conjunto de \textit{core assets}, que são documentos, especificações, componentes e outros artefatos de \textit{software} que possam ser reutilizados durante o desenvolvimento de cada sistema de \textit{software} que compõe a linha de produto \citep{Clements2001}.

O objetivo da \ac{ELPS} é explorar partes comuns em um conjunto de sistemas, enquanto gerencia as partes variáveis, permitindo o desenvolvimento de uma família de sistemas de uma forma rápida e com baixo custo quando comparado ao desenvolvimento de um sistema separadamente \citep{Gomaa2004}. É demonstrado por meio da Figura \ref{fg:literature_review:lps_cost} uma comparação entre o custo de produção de um único produto e o custo de produção de vários produtos.

%\usepackage{graphics} is needed for \includegraphics
\begin{figure}[H]
\centering
  \centerline{\fbox{\includegraphics[width=300px]{chapters/2_literature_review/images/lps_cost.png}}}
  \caption[Custos do desenvolvimento de uma Linha de Produto]{Custos do desenvolvimento de uma Linha de Produto. Adaptado de \citep{Linden2007}}
  \label{fg:literature_review:lps_cost}
\end{figure}

Observando a Figura \ref{fg:literature_review:lps_cost}, a linha sólida representa o custo de desenvolvimento de sistemas de forma independente, enquanto a linha pontilhada representa o custo de desenvolvimento de sistemas utilizando \ac{ELPS}. Como é possível perceber, o custo do desenvolvimento de uma pequena quantidade de sistemas utilizando \ac{ELPS} é relativamente alto e, a medida que a aumenta-se a quantidade de sistema desenvolvidos, o custo de cada sistema desenvolvido diminui e o custo acumulado da produção cresce mais lentamente quando comparado ao custo de desenvolvimento de sistemas individuais.

% =============================================================================================================================

\subsection{Motivação}
\label{sc:linhas_de_produto_de_software:motivacao}

Existem diversas razões para desenvolver uma família de produtos utilizando o paradigma de \ac{ELPS}. Este paradigma proporciona muitos benefícios e, de acordo com \citep{Pohl2005}, estes benefícios consistem em:

\begin{itemize}
  \item Uma vez que muitos produtos são desenvolvidos com base em um conjunto de \textit{core assets} comuns, o \textbf{custo de desenvolvimento e tempo de entrega} de produtos individuais \textbf{são reduzidos} consideravelmente. No entanto, é exigido um investimento inicial e um longo período de tempo para desenvolver os \textit{core assets} que serão reutilizados durante o desenvolvimento de cada produto.
  
  \item Os \textit{core assets} de uma plataforma de \ac{LPS} são reutilizados em muitos produtos. Dessa forma, os \textit{core assets} são revisados, testados e reutilizados muitas vezes, em diferentes cenários. Isto torna mais fácil encontrar e corrigir possíveis problemas, \textbf{aumentando a qualidade} do produto.

  \item Quando os artefatos da plataforma da \ac{LPS} são modificados ou novos artefatos são adicionados, as mudanças são refletidas para todos os produtos derivados desta \ac{LPS}. Isto torna a \textbf{manutenção e evolução mais simples e com custo mais baixo} do que tratar cada produto de forma separada.

  \item Em uma \ac{LPS}, os produtos são customizados de acordo com as necessidades do cliente ou de um segmento específico de mercado. Então, os produtos entregues atendem a necessidades e requisitos individuais com um baixo custo e alta qualidade, proporcionando um \textbf{aumento na satisfação do cliente}.
\end{itemize}

No entanto, apesar de proporcionar estes benefícios, o gerenciamento das variabilidades é essencial para o sucesso de uma linha de produto \citep{Pohl2005}, pois diferentes produtos são desenvolvidos a partir da plataforma da \ac{LPS} em conjunto com uma seleção de variabilidades.

Variabilidades representam diferenças entre os produtos de uma \ac{LPS}, e é por meio do gerenciamento de variabilidades que diversos produtos são construídos a partir de um conjunto de artefatos reutilizáveis. A partir da etapa de configuração do produto, é possível efetuar a ligação entre pontos de variação \textit{(variation point)} e variações \textit{(variant)}. Para ilustrar um exemplo, podemos citar como um ponto de variação o tipo de acesso do sistema e, como exemplo de variação, podemos citar autenticação por usuário e senha, biometria, reconhecimento facial, etc.

Na próxima seção será detalhado o processo de desenvolvimento e as atividades da \ac{ELPS}.

% =============================================================================================================================

\subsection{Engenharia de Linha de Produto de Software}
\label{sc:linhas_de_produto_de_software:elps}

As atividades da \acf{ELPS} são divididas em duas fases principais, que de acordo com \citep{Pohl2005} e \citep{Linden2007}, consistem na Engenharia de Domínio e Engenharia de Aplicação.

\begin{description}
  \item[Engenharia de Domínio] é a fase onde é estabelecida uma plataforma reutilizável e customizável, identificando e definindo as partes comuns e variáveis da linha de produto;

  \item[Engenharia de Aplicação] é a fase em que os produtos são construídos por meio da reutilização dos \textit{core assets} da plataforma e seleção de variabilidades para permitir customizações do produto.

  O processo de engenharia de aplicação é composto por atividades para \textit{(i)} configuração do produto, que consiste na construção de combinações válidas de variabilidades identificadas no processo de engenharia de domínio; \textit{(ii)} derivação do produto, que consiste no processo concreto de construção de uma aplicação da \ac{LPS} \citep{Pohl2005}.
\end{description}

Neste sentido, é necessário utilizar um tipo de processo que considere o reuso de artefatos nas fases de Engenharia de Domínio e Aplicação. \citep{Clements2001} define três importantes atividades para a \ac{ELPS}, conforme apresentado na Figura \ref{fg:literature_review:spl_activities}:

%\usepackage{graphics} is needed for \includegraphics
\begin{figure}[H]
\centering
  \centerline{\fbox{\includegraphics[width=300px]{chapters/2_literature_review/images/spl_activities.png}}}
  \caption[Atividades Essenciais na Engenharia de Linha de Produto de Software]{Atividades Essenciais na Engenharia de Linha de Produto de Software. Adaptado de \citep{Clements2001}}
  \label{fg:literature_review:spl_activities}
\end{figure}

Conforme é possível observar na Figura \ref{fg:literature_review:spl_activities}, as fases consistem no ``Desenvolvimento de \textit{core assets}'', ``Desenvolvimento do Produto'' e ``Gerenciamento''.

\begin{description}
  \item[Desenvolvimento de \textit{core assets}] esta atividade é equivalente à Engenharia de Domínio. Nesta etapa são produzidos um conjunto de \textit{core assets}, um plano de produção e é definido o escopo da linha de produto. É recebido como entrada os pontos comuns e variáveis dos produtos, padrões, requisitos e abordagens para a realização dos \textit{core assets}.

  \item[Desenvolvimento do Produto] esta atividade é equivalente à Engenharia de Aplicação. Recebe como entrada os \textit{core assets} produzidos na fase anterior e os requisitos para a produção de um produto específico.

  \item[Gerenciamento] esta atividade é importante devido a existência de interação entre as etapas de ``Desenvolvimento de \textit{core assets}'' e ``Desenvolvimento do Produto'' \citep{Clements2001}. De acordo com \citep{Pohl2005}, o gerenciamento em um nível técnico e organizacional possui ligação com todo o processo da engenharia da linha de produto, influenciando diretamente no seu sucesso.
\end{description}

Serão apresentadas na próxima seção algumas abordagens utilizadas na adoção do paradigma de \ac{LPS}.

% =============================================================================================================================

\subsection{Modelos de Adoção de Linhas de Produto de Software}
\label{sc:linhas_de_produto_de_software:modelos_adocao}

Muitas organizações são motivadas a adotar o paradigma de \ac{LPS}, principalmente por razões econômicas, pois utilizando as abordagens de \ac{LPS} é possível obter a reutilização de artefatos em larga escala durante o desenvolvimento de \textit{software}, reduzindo consideravelmente os custos de desenvolvimento e tempo de entrega dos produtos \citep{Linden2007}.

Para a adoção bem sucedida do paradigma de \ac{LPS}, é exigido um esforço por parte da organização interessada, que pode utilizar alguns modelos de adoção de acordo com seus objetivos, requisitos,  tempo e orçamento disponível para investimentos. De acordo com \citep{Krueger2002}, os modelos de adoção consistem nas abordagens Pro-ativa, Reativa e Extrativa:

\begin{description}
  \item[Pro-ativa] este modelo de adoção é equivalente ao modelo cascata no desenvolvimento convencional de \textit{software}, onde todas as variações do produto são analisadas, projetadas e, por fim, implementadas. Este modelo é ideal para organizações que têm o domínio completo sobre os requisitos da linha de produto e que possuem tempo e recursos disponíveis para um longo período de desenvolvimento.

  \item[Reativa] neste modelo, as variações do produto são analisadas, projetadas e implementadas de forma incremental, permitindo que o escopo da \ac{LPS} evolua de acordo com a demanda por novos produtos ou novos requisitos em produtos já existentes. Este modelo é ideal para organizações que não conseguem prever os requisitos da linha de produto e que não têm a possibilidade de interromper o desenvolvimento da linha de produto ou estender o tempo durante o processo de adoção.


  \item[Extrativa] a construção da \ac{LPS} é feita a partir da extração de características comuns e variáveis de um conjunto de sistemas de \textit{software} previamente desenvolvidos. Este modelo é ideal para organizações que pretendem migrar o seu modelo de desenvolvimento de \textit{software}, saindo do modelo convencional para o modelo de desenvolvimento de linhas de produto.
\end{description}

Cada um dos modelos de adoção está associado a um conjunto de riscos e benefícios. A abordagem pro-ativa possui mais riscos pois o ciclo de desenvolvimento é longo e requer um investimento inicial considerado alto. No entanto, o retorno de investimento é alto quando comparado com os retornos de investimento das abordagens reativa e extrativa \citep{Alves2005,Clements2001}. Em contrapartida, estas abordagens podem eliminar as barreiras encontradas no processo de adoção, reduzir riscos e permitir uma adoção rápida, pois nem sempre é possível diminuir ou parar a produção de \textit{software} durante a transição \citep{Krueger2002}.

A adoção do paradigma de \ac{LPS} requer um investimento inicial, adequações no processo de desenvolvimento e possivelmente modificações na estrutura organizacional da empresa \citep{Linden2007}. Além disso, é possível citar alguns pré-requisitos para a adoção deste paradigma como utilizar uma tecnologia que permita implementar os conceitos de linhas de produto, processos bem definidos, pessoas que conhecem as necessidades do mercado, a fim de identificar as semelhanças e variações entre os produtos, e um domínio estável de negócio \citep{Pohl2005}. Neste sentido, para que a escolha de um modelo de adoção que possa atender da melhor forma as expectativas da organização, é necessário que haja uma análise de alguns fatores relativos a própria organização como objetivos, tempo e recursos disponíveis.

\subsection{Linhas de Produto de Software Dinâmicas}
\label{sc:linhas_de_produto_de_software:modelos_adocao}

Quando técnicas de \acf{LPS} são aplicadas no desenvolvimento de sistemas adaptativos, tais decisões podem resultar na configuração de um novo produto. Em uma \acs{LPS} tradicional, um produto é derivado de acordo com sua configuração, que ocorre na fase de \textit{design} ou de implementação \citep{Rosenmuller2011} e consiste na seleção de \textit{features} que irão compor o produto, remoção das \textit{features} que não farão parte do produto e ligação dos pontos de variação \citep{Alferez2011}. No entanto, no domínio de sistemas adaptativos, decisões sobre quais são as \textit{features} que irão compor o produto podem estar ligadas a algum atributo ou característica do ambiente, requerendo que a \acs{LPS} seja dinâmica, de modo que tais decisões possam ser adiadas da fase de desenvolvimento para a fase execução \citep{Galster2010,Hallsteinsen2008}. Nesse contexto, diversas pesquisas \citep{Dhungana2007, Hallsteinsen2006, Kim2005, Wolfinger2008}, de diferentes áreas, têm investigado sobre \acfp{LPSD}, que consiste na utilização de abordagens de \acs{LPS} no desenvolvomento de sistemas adaptativos.

O termo ``Linhas de Produto de \textit{Software} Dinâmicas'' \textit{(em inglês, Dynamic Software Product Lines, ou DSPL)} foi introduzido em 2008 por \citep{Hallsteinsen2008}. O autor apresenta um novo conceito que utiliza abordagens de \ac{LPS} para construir sistemas que podem ser adaptados em tempo de execução, de acordo com requisitos do usuário e mudanças no ambiente.

\acfp{LPSD} \citep{Hallsteinsen2008} estendem o conceito convencional de \aclp{LPS} abordando aspectos dinâmicos, provendo uma abordagem para tratar variabilidades que precisam ser manipuladas em tempo de execução \citep{Bencomo2012}. \acsp{LPSD} efetuam \textit{bind} e \textit{unbind} de pontos de variação em tempo de execução. Com isso, é introduzido o conceito de variabilidade dinâmica, possibilitando que o produto derivado de uma \acs{LPS} seja reconfigurado em tempo de execução \citep{Bencomo2010}.

\ac{LPSD} é um termo relativamente novo e diversas pesquisas estão em evolução. Na Seção \ref{sc:trabalhos_relacionados} serão apresentados os principais trabalhos relacionados à nossa abordagem e, por fim, será feita uma discussão sobre as vantagens apresentadas por nossa proposta.

% =============================================================================================================================
% =============================================================================================================================
% =============================================================================================================================

\section{Computação Orientada a Serviços: Uma visão geral}
\label{sc:lps_orientado_a_servicos}

O desenvolvimento de \textit{software} orientado a serviços apresenta uma abordagem para construção de aplicações distribuídas, que possuem suas funcionalidades expostas como serviços para serem utilizadas por outras aplicações ou serviços. Este tipo de aplicação geralmente possui uma arquitetura orientada a serviços \textit{(em inglês, Service-Oriented Architecture, ou SOA)} e permite o reuso de \textit{software}, uma vez que seus artefatos podem ser decompostos em unidades lógicas distintas como serviços e componentes. Alguns estudos \citep{Raatikainen2007,Segura2007,Trujillo2007} têm investigado e discutido a possibilidade de utilizar o modelo de arquitetura orientada a serviços em conjunto com abordages de \ac{LPS}. Com isso, surge o conceito de \ac{LPSOS} \citep{Krut2008}, unindo abordagens de orientação a serviços a \ac{LPS}, para construir aplicações customizadas, de acordo com um segmento específico de mercado.


% =============================================================================================================================
% =============================================================================================================================


\subsection{Arquiteturas Orientadas a Serviços}
\label{sc:lps_orientado_a_servicos:soa}

Nos últimos anos, metodologias para desenvolvimento de sistemas e arquiteturas têm evoluído, partindo de um paradigma de desenvolvimento de aplicações monolíticas e centralizadas para um paradigma de aplicações distribuídas. \ac{COS} \textit{(em inglês, Service-Oriented Computing, ou SOC)} é um paradigma cujo o objetivo é o desenvolvimento de aplicações integradas e implantadas em ambientes distribuídos, utilizando serviços como elementos fundamentais \citep{Papazoglou2003}. De acordo com \citep{Raatikainen2007}, tais serviços devem ser independentes de plataforma, auto descritivos, e autocontidos, de forma que seja possível desenvê-los e implantá-los de forma independente, permitindo a composição de aplicações distribuídas de forma rápida e com baixo custo.

De modo a obter mais eficiência, agilidade e produtividade, o paradigma de computação orientada a serviços por meio de metodologias de desenvolvimento de \textit{software}, utiliza o modelo de \acf{AOS}. Com este é estabelecido um modelo de arquitetura que provê uma infraestrutura entre serviços interconectados por meio de interfaces padronizadas \citep{Papazoglou2003}.

% =============================================================================================================================

\subsubsection{Motivação}
\label{sc:lps_orientado_a_servicos:soa:motivacao}

A adoção do paradigma de computação orientada a serviços não é uma tarefa fácil, e muitas empresas têm falhado neste processo. No entanto, a proposta deste paradigma é bastante atrativa para organizações que desejam obter mais efetividade em suas soluções \citep{Erl2007}. Existem diversos motivos para adotar o modelo de \ac{AOS} e produzir \textit{software} utilizando o paradigma de desenvolvimento orientado a serviços. As principais razões para adotar o modelo de \ac{AOS} são:


\begin{itemize}
  \item Reusabilidade: a reusabilidade é um dos principais objetivos da Engenharia de \textit{Software} e, no contexto de \ac{AOS}, a possibilidade de compor novos serviços reutilizando serviços já existentes provê algumas vantagens para a organização como Retorno de Investimento \textit{(em inglês, Return on Investiments, ou ROI)}, qualidade de \textit{software}, diminuição de custos e tempo de entrega \citep{Elfatatry2004}.

  \item Flexibilidade e Agilidade: constantemente os sistemas de \textit{software} precisam passar por modificações para atender novos requisitos, que são requeridos por novas exigências de mercado \citep{Carter2007}. Por isso, a arquitetura do sistema de \textit{software} deve ser reconfigurável, de uma forma simples. As características do modelo de \ac{AOS} permitem um melhor alinhamento entre o sistemas de \textit{software} e as necessidades de mercado por meio da integração de novos serviços e reuso de serviços existentes, possibilitando responder rapidamente a possíveis mudanças nos requisitos \citep{Endrei2004}.

  \item Integração e Interoperabilidade: serviços podem ser desenvolvidos de forma independente e então são compostos com outros serviços a fim de prover funcionalidades de uma aplicação que pode ser facilmente adaptada a novos requisitos e, por meio da interoperabilidade, serviços heretogêneos são facilmente integrados, permitindo a troca de mensagem entre estes \citep{Erl2005,Ziyun2011}.
\end{itemize}

O modelo de arquitetura orientada a serviços normalmente é definido com base em um conjunto de princípios \citep{Erl2005}. Serão apresentados na próxima seção os princípios que são diretamente relacionados com este modelo de arquitetura.

% =============================================================================================================================

\subsubsection{Princípios e Papéis}
\label{sc:lps_orientado_a_servicos:soa:principios}

Conforme mencionado anteriormente, o modelo de arquitetura orientada a serviços é definido com base em um conjunto de princípios que apoiam suas características. Os princípios que são diretamente relacionados com este modelo de arquitura são:

\begin{itemize}

  \item Acoplamento: este princípio se refere ao número de dependências entre serviços. O baixo acoplamento é obtido por meio do uso de padrões e contratos de serviços entre clientes e provedores, que permitem a interação dos serviços utilizando interfaces bem definidas. O princípio do acoplamento também afeta outro atributos de qualidade como a modificabilidade e reusabilidade da aplicação \citep{McGovern2003}.

  \item Contrato de Serviço: serviços aderem a contratos de comunicação, definidos por um ou mais serviços. Neste contrato são definidos formato de dados, regras e características dos serviços e suas funcionalidades. Para permitir estas definições, são utilizados formatos padronizados como XML, WSDL e XSD \citep{Erl2005,Dirksen2013}.

  \item Autonomia e Abstração: os serviços devem ser autônomos e auto-contidos, tendo o controle sobre a lógica que é encapsulada por eles e provendo uma abstração de suas funcionalidades por meio do seu contrato de serviço \citep{Erl2005}.

  \item Descoberta de Serviços: serviços são especificados de uma forma descritiva para permitir a sua localização por meio de mecanismos de descoberta de serviços, como o UDDI\footnote{http://uddi.xml.org} \textit{(Universal Description, Discovery and Integration)} \citep{Erl2005} e o WS-Discovery \citep{WSDISCOVERY2005}.

  \item Alta Granularidade: serviços provêem abstrações que suportam a separação de interesses de domínio \textit{(separation of concerns)} e visibilidade de informação \textit{(information hidding)}. Devido a existência de chamadas remotas, a performance do serviço é comprometida e, por esta razão, os serviços devem prover funcionalidades de alta granularidade, diminuindo a necessidade de várias chamadas remotas para resolver uma única requisição.

\end{itemize}

O modelo de arquitetura orientada a serviços não trata apenas aspectos sobre a definição de serviços, mas também provê definições de papéis e suas responsabilidades. De acordo com \citep{Erl2007} e \citep{Papazoglou2003}, são definidos quatro papéis, conforme apresentado na Figura \ref{fg:lps_orientado_a_servicos:soa:principios:papeis_e_operacoes}.

%\usepackage{graphics} is needed for \includegraphics
\begin{figure}[H]
\centering
  \centerline{\fbox{\includegraphics[width=400px]{chapters/2_literature_review/images/soa_roles.png}}}
  \caption[Arquitetura Orientada a Serviços: Papéis e Operações]{Arquitetura Orientada a Serviços: Papéis e Operações. Adaptado de \citep{Papazoglou2003}}
  \label{fg:lps_orientado_a_servicos:soa:principios:papeis_e_operacoes}
\end{figure}

Conforme a Figura \ref{fg:lps_orientado_a_servicos:soa:principios:papeis_e_operacoes}, os papéis consistem em: Cliente \textit{(Service Consumer)}, Provedor de Serviço \textit{(Service Provider)}, Registro e Descoberta de Serviços \textit{(Service Registry)} e o Contrato de Serviço \textit{(Service Contract)}. Também é possível observar nesta mesma figura, os relacionamentos entre os papéis elencados acima.

\begin{itemize}

  \item Cliente: pode ser uma aplicação, um serviço ou outra entidade de \textit{software} que necessita acessar o serviço. Este acesso pode ser possibilitado diretamente por meio do URI \textit{(Uniform Resource Identifier)} ou por meio da descoberta de serviços, utilizando o registro de serviço \citep{Josuttis2007}.

  \item Provedor de Serviço: é a entidade que recebe e processa as requisições efetuadas pelo Cliente. Esta entidade pode ser uma aplicação legada, um componente ou outra entidade de \textit{software} que expõe sua interface como serviço \citep{Josuttis2007}, provendo uma implementação para um especificação ou contrato de serviço. As informações sobre provedores de serviços são publicadas no registro de serviços \citep{McGovern2003}.

  \item Registro de Serviço: é entidade responsável por manter as informações sobre contratos de serviços e provedores. Esta entidade pode ser utilizada para implementação de atributos de qualidade de serviços como disponibilidade e modificabilidade \citep{Gonzalez2010}.

  \item Contrato de Serviços: por meio do contrato de serviço é especificado a forma como o Cliente irá interagir com o Provedor do Serviço. Este contrato especifica o formato das mensagens, condições para acessar o Provedor do serviço e pode descrever atributos de qualidade \citep{Erl2005,McGovern2003}.

\end{itemize}


% =============================================================================================================================
% =============================================================================================================================


\subsection{Linhas de Produto Orientadas a Serviços}
\label{sc:lps_orientado_a_servicos:sopl}

Os paradigmas de \ac{AOS} e \ac{LPS} possuem um objetivo em comum, que é a motivação por reutilização de artefatos em vez de desenvolvê-los do início. Isto implica ganhos em produtividade, redução de custos e tempo de entrega. \ac{LPS} explora características comuns e variáveis entre um conjunto de sistemas e o paradigma de \ac{AOS} possui características que são requeridas por muitas \acp{LPS} \citep{Yu2010}, provendo uma estrutura dinâmica e desacoplada, de modo que possibilite obter mais flexibilidade e maior poder de adaptação a novos cenários \citep{Ye2007}. Com o objetivo de construir artefatos de \textit{software} cada vez mais reutilizáveis, diversas abordagens \citep{Galster2010, Raatikainen2007, Segura2007, Smith2009, Trujillo2007} têm investigado o uso do modelo de \acf{AOS} em conjunto com abordagens de \ac{LPS}.

Algumas abordagens propostas \citep{Istoan2009,Medeiros2010,Ribeiro2010} que tratam questões relacionadas a Linhas de Produto Orientadas a Serviços, consideram unidades de serviços como \textit{features}, conforme a Figura \ref{fg:lps_orientado_a_servicos:sopl:sopl}.

%\usepackage{graphics} is needed for \includegraphics
\begin{figure}[H]
\centering
  \centerline{\fbox{\includegraphics[width=400px]{chapters/2_literature_review/images/SOPL.png}}}
  \caption[Linha de Produto Orientadas a Serviços: Configuração de Produtos]{Linha de Produto Orientadas a Serviços: Configuração de Produtos}
  \label{fg:lps_orientado_a_servicos:sopl:sopl}
\end{figure}

Conforme apresentado na Figura \ref{fg:lps_orientado_a_servicos:sopl:sopl}, cada unidade de serviço é considerada uma \textit{feature}, que pode compor a configuração do produto, respeitando restrições que determinam os serviços como Obrigatório, Opcional ou Alternativo.

Na abordagem proposta neste trabalho, é considerado que os serviços são partes de uma \textit{feature}. Ou seja, conforme apresentado na Figura \ref{fg:lps_orientado_a_servicos:sopl:sopl_dymos}, uma \textit{feature} pode ser composta por um ou mais serviços e um conjunto de implementações que utilizam tais serviços para prover funcionalidades. 

%\usepackage{graphics} is needed for \includegraphics
\begin{figure}[H]
\centering
  \centerline{\fbox{\includegraphics[width=150px]{chapters/2_literature_review/images/SOPL_DYMOS.png}}}
  \caption[Composição de \textit{features}]{Composição de \textit{features}}
  \label{fg:lps_orientado_a_servicos:sopl:sopl_dymos}
\end{figure}

Com essa definição, foi possível estabelecer um alinhamento entre as adaptações da aplicação Cliente e os serviços disponíveis no lado Servidor. Esse alinhamento foi possibilitado por meio de reconfigurações na composição de serviços, de acordo com as \textit{features} adaptadas na aplicação Cliente.


% =============================================================================================================================
% =============================================================================================================================

\section{Trabalhos Relacionados}
\label{sc:trabalhos_relacionados}

Durante o desenvolvimento deste trabalho foi possível identificar alguns trabalhos relacionados. Estes trabalhos são descritos abaixo:

\citep{Parra2009} propõe uma Linha de Produto Dinâmica, Orientada a Serviço e Sensível ao Contexto denominada CAPucine. Esta linha de produto possui uma abordagem orientada a modelos e é dividida em dois processos para a execução da derivação do produto. No primeiro processo, todas as \textit{features} selecionadas são associadas ao artefato que corresponde ao modelo parcial do produto, que é composto e transformado para gerar o produto. O segundo processo é referente a adaptações dinâmicas, que são efetuadas utilizando o FraSCAti\footnote{http://frascati.ow2.org/}. O FraSCAti é uma plataforma de Arquitetura de Componentes Orientados a Serviço \textit{(em inglês, Service Component Architecture, ou SCA)} com propriedades dinâmicas que permite operações de \textit{bind} e \textit{unbind} de componentes em tempo de execução.

\citep{Yu2010} propõe uma abordagem que é dividida em três fases e une conceitos de \ac{LPS} a \ac{COS} para desenvolver composições dinâmicas de serviços. A primeira fase da abordagem consiste em especificar a arquitetura da linha de produto, efetuando a sua integração com o modelo de variabilidades. A segunda fase consiste em definir composições de serviços, que devem seguir a arquitetura da linha de produto. A terceira fase consiste em criar e manter de forma autônoma a aplicação construída com base na arquitetura da linha de produto.

\citep{Alferez2011} propõe uma abordagem para projetar e implementar \textit{Web Services} Sensíveis ao Contexto em uma família de sistemas. A abordagem proposta é apoiada pelo (MoRE-WS), que é uma plataforma de reconfiguração guiada por modelos, onde são utilizados modelos de variabilidades como políticas de adaptação para gerar e executar de forma automática o plano de reconfiguração.

A partir destes trabalhos foram identificadas algumas limitações na tentativa de aplicá-los no cenário de \ac{LPSOSSC} que seguem o modelo de \acs{AOS} utilizando o padrão arquitetural Cliente-Servidor \citep{Erl2007}, onde \textit{features} que compõem o produto no lado cliente podem endereçar uma composição de serviços. 

Uma limitação encontrada no trabalho proposto por \citep{Parra2009} diz respeito a complexidade da abordagem, que dificulta a sua utilização em sistemas de pequeno porte ou que estão na fase inicial da adoção do paradigma de \ac{LPS}. No trabalho proposto por \citep{Alferez2011}, foi possível observar uma limitação que consiste em considerar um serviço como uma única \textit{feature}, efetuando adaptações dinâmicas por meio da substituição de seus provedores. Neste caso, não são tratadas as \textit{features} que são compostas não apenas por serviços, mas também por outros artefatos que utilizam tais serviços para prover suas funcionalidades.

Por fim, conforme colocado por \citep{Jackson2013}, os reconfiguradores utilizados pelas abordagens atuais, inclusive \citep{Yu2010}, apresentam um forte acoplamento com sua respectiva \ac{LPSD}.

No próximo capítulo (Capítulo \ref{ch:the_solution}) será apresentada a solução proposta neste trabalho, detalhando como a solução trata as limitações descritas nesta seção.
