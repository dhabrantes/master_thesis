\chapter{Introdução}
\label{ch:introducao}

\begin{quotation}[]{Confucius}
A vida é simples, mas nós insistimos em complicá-la.\\
Life is really simple, but we insist on making it complicated.
\end{quotation}

% =============================================================================================================================
% =============================================================================================================================
% =============================================================================================================================

\section{Motivação}
\label{sc:motivacao}

É notório o surgimento de ambientes cada vez mais dinâmicos, exigindo sistemas mais flexíveis, de forma que componentes possam ser plugados ou desplugados durante o seu ciclo de vida, inclusive em tempo de execução \citep{Kim2005,Niebuhr2009}. Para atender estes requisitos, é necessário que decisões sobre possíveis adaptações e variações do sistema possam ser tomadas em tempo de execução. \acp{SSC} atendem a esse propósito, sendo capazes de adaptar-se em tempo de execução de acordo com mudanças no ambiente, obedecendo um conjunto de regras \citep{Hallsteinsen2006,Kim2005,Oreizy1999}. 

Quando técnicas de \acf{LPS} são aplicadas no desenvolvimento de sistemas adaptativos, tais decisões podem resultar na configuração de um novo produto. Em uma \acs{LPS} tradicional, um produto é derivado de acordo com sua configuração, que ocorre na fase de \textit{design} ou de implementação \citep{Rosenmuller2011} e consiste na seleção de \textit{features} que irão compor o produto, remoção das \textit{features} que não farão parte do produto e ligação dos pontos de variação \citep{Alferez2011}. No entanto, no domínio de sistemas adaptativos, decisões sobre quais são as \textit{features} que irão compor o produto podem estar ligadas a algum atributo ou característica do ambiente, requerendo que a \acs{LPS} seja dinâmica, de modo que tais decisões possam ser adiadas da fase de desenvolvimento para a fase de execução \citep{Galster2010,Hallsteinsen2008}. Neste contexto, diversas pesquisas \citep{Dhungana2007, Hallsteinsen2006, Kim2005, Wolfinger2008}, de diferentes áreas, têm investigado sobre a utilização de abordagens de \acs{LPS} no desenvolvimento de sistemas adaptativos.

\acp{LPSD} \citep{Hallsteinsen2008} estendem o conceito convencional de \aclp{LPS} abordando aspectos dinâmicos e provendo uma abordagem para tratar variabilidades que precisam ser manipuladas em tempo de execução \citep{Bencomo2012}. \acsp{LPSD} efetuam \textit{bind}\footnote{associação de uma variante a um ponto de variação} e \textit{unbind}\footnote{dissociação de uma variante a um ponto de variação} de pontos de variação em tempo de execução. Com isso, é introduzido o conceito de variabilidade dinâmica, possibilitando que o produto derivado de uma \acs{LPS} seja reconfigurado em tempo de execução \citep{Bencomo2010}.

Tecnologias orientadas a serviços possuem características que são requeridas por muitas \ac{LPS} \citep{Yu2010}, provendo uma estrutura dinâmica e desacoplada, de modo que possibilite obter mais flexibilidade e maior poder de adaptação a novos cenários \citep{Ye2007}. Este tipo de tecnologia é utilizado para o desenvolvimento de \textit{software} como serviço, resultando em componentes com baixo acoplamento e interoperabilidade entre linguagens e plataformas \citep{Demirkan2008}. Com o objetivo de construir artefatos de \textit{software} cada vez mais reutilizáveis, diversas abordagens \citep{Galster2010, Raatikainen2007, Segura2007, Smith2009, Trujillo2007} têm investigado o uso do modelo de \acf{AOS} para este propósito. Com isso, surge o conceito de \ac{LPSOS} \citep{Krut2008}, unindo práticas de orientação a serviços a \ac{LPS}, para construir aplicações customizadas, de acordo com um segmento específico de mercado. Este modelo de arquitetura possui características dinâmicas e flexíveis que podem ajudar a obter uma gerência de variabilidades mais otimizada \citep{Galster2010}.

\acs{LPSOS} representa uma categoria de \acs{LPSD}, que é desenvolvida seguindo uma arquitetura orientada a serviços e, do ponto de vista da \ac{ELPS}, é possível perceber benefícios como o suporte contínuo a mudanças no contexto e a possibilidade de combinar serviços em várias configurações, simplificando a implantação de pontos de variação que, por exemplo, embora utilizem a mesma base de serviços, necessitam passar por adaptações para atender a diferentes requisitos \citep{Lee2012}. Portanto, é importante a existência de técnicas de engenharia de \textit{software} que apoie o desenvolvimento de sistemas com baixo acoplamento, dinâmicos e flexíveis \citep{Ali2009}. 

Diversas soluções para desenvolvimento de sistemas sensíveis ao contexto foram propostas \citep{Kon2000,Rocha2007,Viana2008}. No entanto, apesar de algumas dessas abordagens proverem mecanismos que permitem o reuso de artefatos, elas não são aplicadas de forma sistemática durante as fases de desenvolvimento de \textit{software} \citeauthor{Marinho2012}. Em contrapartida, \citeauthor{Marinho2012} e \citeauthor{Parra2012} propõem abordagens sistemáticas para desenvolvimento desse tipo de sistema, onde o primeiro autor utiliza técnicas de \acs{LPS} e o segundo autor combina técnicas de \acs{LPS} e \acs{AOS}.

Quando alinhamos paradigmas como Sistemas Sensíveis ao Contexto, \acs{AOS} e \acs{LPS}, podemos enfrentar alguns desafios. O sistema derivado de uma \ac{LPS} é composto por \textit{features} e pontos de variação. Considerando que o modelo de \acs{AOS} segue o padrão arquitetural Cliente-Servidor \citep{Erl2007}, podemos ter um cenário em que as \textit{features} que compõem o produto no lado cliente podem endereçar uma composição de serviços, que está em execução no lado servidor. Dessa forma, os pontos de variação podem sofrer variabilidades de acordo com mudanças no contexto, de modo que \textit{features} da aplicação cliente sejam ativadas ou desativadas, exigindo a execução de reconfigurações nos serviços de modo a atender tais variabilidades. As abordagens propostas atualmente não oferecem um suporte para esse tipo de problema ou são incipiente, estando em fases iniciais de pesquisa.

Portanto, é possível perceber a necessidade de um \textit{framework} que permita a reconfiguração de tais variabilidades, efetuando reconfigurações dinâmicas em \aclp{LPSOSSC}.

%The remainder of this chapter describes the focus of this dissertation and
%starts by presenting its motivation in \secref{sc:motivation} and a clear
%definition of the problem in \secref{sc:problem}. An overview of the proposed
%solution is presented in \secref{sc:solution}, while \secref{sc:outofscope}
%describes some related aspects that are not directly addressed by this work.
%\secref{sc:contributions} presents the main contributions and, finally,
%\secref{sc:structure} describes how this dissertation is organized.

% =============================================================================================================================
% =============================================================================================================================
% =============================================================================================================================

\section{Problemática}
\label{sec:problematica}

O principal desafio quando utilizamos técnicas de \ac{LPS} no desenvolvimento de \acp{SSC} é a necessidade de adiar decisões que ocorreriam na fase de \textit{design} ou de implementação para a fase de execução (\textit{runtime}). Tais decisões influenciam diretamente nos artefatos de \textit{software} que compõem o produto.

De acordo com \citep{Pohl2005}, um produto derivado de uma \ac{LPS} é composto por características (\textit{features}) comuns e variáveis (\textit{variabilities}). Desse modo, no cenário de \acl{LPSOSSC}, o produto pode ser composto por \textit{features} que, para entrar em execução, precisam utilizar um serviço ou uma composição deles. 

Se considerarmos serviços como parte de uma \textit{feature}, ao ocorrer uma ativação ou desativação de uma ou mais \textit{features} no sistema, e em tempo de execução, é necessário efetuar uma reconfiguração na composição dos serviços, de modo que a adaptação das \textit{features} e mudança no contexto seja atendida \citep{Alferez2011}.

\begin{inparaenum}[\itshape(i)\upshape]
Dessa forma, faz-se necessário um \textit{framework} que seja responsável por tratar reconfigurações de tais variabilidades. De acordo com \citep{Erl2007}, o modelo de \acs{AOS} segue o padrão arquitetural Cliente-Servidor. Então, como o cliente e o servidor são executados em ambientes diferentes, é necessário que o \textit{framework} seja capaz de \textbf{\item\label{sc:problematica_p1}} receber notificações sobre mudanças na composição de \textit{features} da aplicação cliente, para que \textbf{\item\label{sc:problematica_p2}} reconfigurações possam ser efetuadas no lado servidor.

\acsp{SSC} são flexíveis, dinâmicos e se adaptam em tempo de execução de acordo com mudanças no ambiente, obedecendo um conjunto de regras \citep{Hallsteinsen2006,Kim2005,Oreizy1999}. Quando mudanças ocorrem, reconfigurações são executadas, tornando disponível ou indisponível um ou mais serviços, compondo uma plataforma de serviços dinâmica. Considerando que os serviços estarão em execução em uma plataforma com características dinâmicas, é necessário um \textbf{\item\label{sc:problematica_p3}} mecanismo de descoberta de serviços, que insira uma camada de abstração entre o cliente, o servidor e o serviço requisitado. 

Com isso, espera-se que haja um menor acoplamento e maior flexibilidade, visto que não devem existir requisições para um serviço específico, de forma fixa, mas para um serviço que atenda a uma determinada especificação. O mecanismo de descoberta de serviços deve ser responsável por selecionar com base em critérios de seleção o serviço mais adequado e retornar a sua referência (\textit{endpoint}) para que possa ser acessado.
\end{inparaenum}

%Passos: 
%Mobile: observar o contexto; notificar o servidor; consumir serviços
%Server: receber notificações e executar reconfiguracoes; responder a requisicao de serviços

%Com a aplicação \textit{mobile} em execução, ela deve ser responsável por observar mudanças no contexto, e então, notificar o servidor, onde os serviços estarão disponíveis.

%Uma vez que o servidor é notificado, será executada uma reconfiguração para atender a novos requisitos do contexto. Após a execução da reconfiguração, os serviços devem estar ativos e acessíveis. 

%Se tratando de sistemas sensíveis ao contexto, diversas mudanças podem ocorrer para atender a novos requisitos. Quando mudanças ocorrem, reconfigurações são executadas, tornando disponível ou indisponível um ou mais serviços, compondo uma plataforma de serviços dinâmica. Dessa forma, faz-se necessário um mecanismo para recuperar referências (\textit{endpoint's}) dos serviços que atendem a nova configuração do produto.

%The decisions about commonalities and variabilities must be moved from design time to runtime, in order to perform a dynamic product reconfiguration.


%ok 1- The main challenge when join context-aware applications and SPL approach is to delay design decision as late as possible. 

% 2- In scenario of service-oriented product line, the client application must be responsible to watch context changes, and then, notify the server-side.

% 3- Once the server-side was notified, it must perform a reconfiguration to attend new requirements in context.

% 4- After performing an reconfiguration, the services must be accessible. So, there must be a way to retrieve a service references of new product configuration.


% In fact:
% - The main challenge is to delay design decision as late as possible. 
% - The mobile application must be responsible to watch and notify.
% - A reconfiguration must be performed at server-side.
% - The services of new product configuration must be accessible through a mechanism that finds service references.

O problema investigado pode ser expressado por meio de perguntas de pesquisa, nas quais suas respostas definem a abordagem a ser investigada no trabalho. A abordagem utilizada neste trabalho é direcionada para responder a seguinte pergunta:


% \begin{itemize}
% 	\item Em um cenário que segue o modelo de \acfp{AOS} como \acfp{LPSOSSC}, considerando que as \textit{features} que compõem o produto no lado Cliente estão ligadas a Serviços que estão disponíveis no lado Servidor, como é possível permitir a reconfiguração dinâmica de variabilidades que são compostas por este tipo de \textit{feature}?
% \end{itemize}

\begin{itemize}
	\item Como é possível permitir reconfigurações de variabilidades dinâmicas em \acfp{LPSOSSC}?
\end{itemize}

Portanto, como forma de obter uma resposta para esta pergunta de pesquisa,

\begin{quote}
\emph{
  Neste trabalho é apresentado um estudo sobre variabilidades dinâmicas em \acl{LPSOSSC}, investigando especificamente situações quando \textit{features} que endereçam um ou mais serviços são reconfiguradas no lado cliente, requerendo reconfigurações nos serviços, no lado servidor. Como resultado deste estudo é provido um \textit{framework} para suporte a reconfiguração e descoberta de serviços.
}
\end{quote}

% =============================================================================================================================
% =============================================================================================================================
% =============================================================================================================================

\section{Visão Geral da Solução}
\label{sec:intro:visao_geral_solucao}
De modo a permitir variabilidades dinâmicas em \acp{LPSOSSC}, foi proposto e desenvolvido o DYMOS \textit{Framework}. É apresentado por meio da da Figura \ref{fg:layers}, a forma como o DYMOS está disposto em relação as demais tecnologias utilizadas.

O DYMOS provê, de uma forma desacoplada, uma plataforma de reconfiguração de variabilidades dinâmicas e descoberta de serviços para Linhas de Produto de \textit{Software} Orientado a Serviços e Sensível ao Contexto. O DYMOS oferece algumas vantagens como: possibilidade de Gerência de variabilidades de uma forma leve e simples, permitindo que serviços sejam adicionados ou removidos da configuração do produto em tempo de execução por meio de mecanismos de auto-implantação, de modo que o funcionamento do sistema não seja afetado e que tais modificações sejam aplicadas e reconhecidas imediatamente; Mecanismo de Descoberta de Serviço, que provê uma abstração entre o Cliente e o Serviço, permitindo agregar critérios para a seleção do serviço mais adequado, de acordo com uma requisição; As principais funcionalidades do DYMOS estão dispostas na forma de serviços \textit{web} e, por meio destes, é obtida a característica de interoperabilidade, permitindo que o \textit{framework} seja utilizado por aplicações desenvolvidas em outras plataformas.

De acordo com \citep{Gomaa2011}, é possível classificar uma reconfiguração em tempo de execução como ``comportamental'', ``arquitetural'' ou ``reconfiguração de componentes''. Uma adaptação comportamental ocorre quando o comportamento do sistema é alterado, mantendo a mesma estrutura e arquitetura. Uma adaptação arquitetural ocorre quando a arquitetura do sistema é impactada diretamente pela adaptação. Uma adaptação de componentes envolve uma substituição de um componente por outro que implemente a mesma interface. O tipo de reconfiguração executada pelo DYMOS se trata de reconfiguração de componentes.

O \textit{framework} foi desenvolvido de forma modular utilizando tecnologias OSGi\footnote{http://www.osgi.org}. Como forma de obter componentes modularizados, foi utilizado o iPOJO\footnote{http://felix.apache.org/site/apache-felix-ipojo.html}, que é um \textit{framework} para desenvolvimento de componentes orientados a serviço para compor sistemas modulares que requerem adaptações em tempo de execução \citep{Escoffier2007}.
Como plataforma de execução, é utilizado o \textit{framework} Apache Felix\footnote{http://felix.apache.org/}, que é uma implementação do OSGi \textit{Service Platform}. Para expor funcionalidades por meio de serviços \textit{web}, é utilizado o OSGi Distribuído\footnote{http://cxf.apache.org/distributed-osgi.html}, que é um subprojeto do Apache CXF\footnote{http://cxf.apache.org}.



%\usepackage{graphics} is needed for \includegraphics
\begin{figure}[H]
\centering
  \fbox{\includegraphics[width=250px]{chapters/1_introduction/layers.png}}
  \caption[DYMOS: Arquitetura Externa]{DYMOS: Arquitetura Externa}
  \label{fg:layers}
\end{figure}

Com o objetivo de abordar os itens \ref{sc:problematica_p1}, \ref{sc:problematica_p2} e \ref{sc:problematica_p3} destacados na Seção \ref{sec:problematica}, os seguintes itens foram desenvolvidos em forma de componentes como parte da solução:

\begin{description}
  \item[Descritor de Serviços] permite descrever serviços, indicando um ID (identificador), especificações, implementações e regras que são aplicadas quando a operação de descoberta de serviço é solicitada. Quando há uma requisição, tais regras devem ser utilizadas a fim de selecionar o serviço adequado.

  \item[Descritor de Variabilidades] permite descrever variabilidades, indicando pontos de variação. A descrição consiste na atribuição de um ID e um mapeamento entre pontos de variação e um ou mais serviços.

  \item[Mecanismo de Reconfiguração de Serviços] é responsável por reconfigurar os serviços para atender a mudanças no ambiente. A reconfiguração é feita em termos de ativação ou desativação de um ou mais serviços, tornando-os disponíveis ou indisponíveis para utilização. Este componente atende ao item \ref{sc:problematica_p2}, mencionado na Seção \ref{sec:problematica}.

  \item[Mecanismo de Descoberta de Serviços] é responsável por prover operações de descoberta de serviços. Essas operações possibilitam a recuperação de referências (\textit{endpoint}) de acesso a serviços, informando o ID atribuído na descrição do serviço. Este mecanismo deve selecionar o serviço mais adequado, de acordo com o ID informado e, em seguida, deve ser retornado o \textit{endpoint} do serviço para que possa ser utilizado pelo cliente. Com isso, serviços podem ser inseridos ou removidos a qualquer momento, sem haver a necessidade de alteração de código no cliente ou no servidor.

\end{description}

Os itens ``Descritor de Serviços'' e ``Descritor de Variabilidades'' são utilizados pelos mecanismos de Reconfiguração e Descoberta de Serviços, de modo que as operações de reconfiguração e descoberta de serviços sejam efetuadas de acordo com cada uma das descrições.
De modo a permitir os Descritores de Serviços e de Variabilidades, foi definida e disponibilizada uma sintaxe baseada em XML, que deverá ser utilizada para especificar cada um dos itens.

As operações de reconfiguração e descoberta de serviços que são providas respectivamente pelo ``Mecanismo de Reconfiguração de Serviços'' e ``Mecanismo de Descoberta de Serviços'', estão disponíveis por meio de Serviços \textit{Web}. O Serviço \textit{Web} referente operações de reconfiguração permite que o \textit{framework} seja notificado sobre mudanças no contexto, atendendo ao item \ref{sc:problematica_p1}, mencionado na Seção \ref{sec:problematica}

Uma descrição mais detalhada sobre a solução proposta é apresentada no Capítulo \ref{ch:the_solution}.

% =============================================================================================================================
% =============================================================================================================================
% =============================================================================================================================

\section{Escopo Negativo}
\label{sc:escopo_negativo}

Alguns aspectos que são relacionados a esta pesquisa não são considerados como parte do escopo deste trabalho. Este trabalho pode ser visto como um passo inicial de uma pesquisa sobre Reconfiguração Dinâmica de Variabilidades em \acfp{LPSOSSC}. Assim, alguns pontos não são diretamente endereçados por este trabalho:


\begin{description}
  \item[Agente] Para que um sistema possa adaptar-se, é preciso que algum componente notifique sobre uma possível mudança no contexto. Isso torna necessário a implementação de agentes, que são responsáveis por observar o contexto e informar sobre tais mudanças. Nossa solução oferece um conjunto de mecanismos que são responsáveis por receber notificações sobre adaptações.

  \item[Interpretação de Regras de Contexto] Para determinar quais são os componentes que serão adaptados, algumas regras definidas previamente devem ser consideradas. No entanto, quando ocorre uma notificação de mudança no contexto, a aplicação cliente deve ser responsável por analisar tais regras e notificar a nossa solução, indicando quais os pontos de variação serão afetados.

\end{description}

% =============================================================================================================================
% =============================================================================================================================
% =============================================================================================================================

\section{Organização da Dissertação}
\label{sc:organizacao_dissertacao}

A estrutuda dessa dissertação está organizada da seguinte forma:

No Capítulo \ref{ch:literature_review} é apresentada uma revisão da literatura, de modo a efetuar uma contextualização sobre as principais áreas abordadas neste trabalho.
No Capítulo \ref{ch:the_solution} é apresentada em detalhes a solução proposta neste trabalho, destacando os requisitos elencados, a função de cada componente da solução e como estes componentes permitiram atender os requisitos.
No Capítulo \ref{ch:avaliacao_preliminar} é apresentada uma avaliação preliminar da abordagem proposta por meio de um estudo de caso.
No Capítulo \ref{ch:conclusao} são apresentadas as Considerações Finais, Principais Contribuições e Trabalhos Futuros.














