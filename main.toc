\select@language {brazil}
\contentsline {chapter}{\numberline {1}Introdu\IeC {\c c}\IeC {\~a}o}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Motiva\IeC {\c c}\IeC {\~a}o}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Problem\IeC {\'a}tica}{3}{section.1.2}
\contentsline {section}{\numberline {1.3}Vis\IeC {\~a}o Geral da Solu\IeC {\c c}\IeC {\~a}o}{5}{section.1.3}
\contentsline {section}{\numberline {1.4}Escopo Negativo}{7}{section.1.4}
\contentsline {section}{\numberline {1.5}Organiza\IeC {\c c}\IeC {\~a}o da Disserta\IeC {\c c}\IeC {\~a}o}{8}{section.1.5}
\contentsline {chapter}{\numberline {2}Revis\IeC {\~a}o da Literatura}{9}{chapter.2}
\contentsline {section}{\numberline {2.1}Linhas de Produto de Software: Uma vis\IeC {\~a}o geral}{9}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Motiva\IeC {\c c}\IeC {\~a}o}{10}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Engenharia de Linha de Produto de Software}{11}{subsection.2.1.2}
\contentsline {subsection}{\numberline {2.1.3}Modelos de Ado\IeC {\c c}\IeC {\~a}o de Linhas de Produto de Software}{13}{subsection.2.1.3}
\contentsline {subsection}{\numberline {2.1.4}Linhas de Produto de Software Din\IeC {\^a}micas}{14}{subsection.2.1.4}
\contentsline {section}{\numberline {2.2}Computa\IeC {\c c}\IeC {\~a}o Orientada a Servi\IeC {\c c}os: Uma vis\IeC {\~a}o geral}{15}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Arquiteturas Orientadas a Servi\IeC {\c c}os}{16}{subsection.2.2.1}
\contentsline {subsubsection}{Motiva\IeC {\c c}\IeC {\~a}o}{16}{subsubsection*.16}
\contentsline {subsubsection}{Princ\IeC {\'\i }pios e Pap\IeC {\'e}is}{17}{subsubsection*.17}
\contentsline {subsection}{\numberline {2.2.2}Linhas de Produto Orientadas a Servi\IeC {\c c}os}{19}{subsection.2.2.2}
\contentsline {section}{\numberline {2.3}Trabalhos Relacionados}{21}{section.2.3}
\contentsline {chapter}{\numberline {3}DYMOS Framework}{23}{chapter.3}
\contentsline {section}{\numberline {3.1}Vis\IeC {\~a}o Geral da Solu\IeC {\c c}\IeC {\~a}o}{23}{section.3.1}
\contentsline {section}{\numberline {3.2}Elicita\IeC {\c c}\IeC {\~a}o de Requisitos}{26}{section.3.2}
\contentsline {section}{\numberline {3.3}Tecnologias Utilizadas}{27}{section.3.3}
\contentsline {section}{\numberline {3.4}Arquitetura e Implementa\IeC {\c c}\IeC {\~a}o}{28}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Componentes Descritores}{29}{subsection.3.4.1}
\contentsline {subsubsection}{Descritor de Servi\IeC {\c c}os}{31}{subsubsection*.23}
\contentsline {subsubsection}{Descritor de Variabilidades}{33}{subsubsection*.24}
\contentsline {subsubsection}{Web Service Resolver}{35}{subsubsection*.25}
\contentsline {subsection}{\numberline {3.4.2}Componente ServiceContext}{36}{subsection.3.4.2}
\contentsline {subsubsection}{Reconfigurando Variabilidades Din\IeC {\^a}micas}{37}{subsubsection*.27}
\contentsline {subsubsection}{Descoberta de Servi\IeC {\c c}os}{39}{subsubsection*.28}
\contentsline {subsubsection}{Reconstruindo o ServiceContext com o ContextHotBuild}{43}{subsubsection*.29}
\contentsline {subsection}{\numberline {3.4.3}Componente ApplicationService}{44}{subsection.3.4.3}
\contentsline {subsection}{\numberline {3.4.4}Decis\IeC {\~o}es Arquiteturais e Conclus\IeC {\~a}o}{45}{subsection.3.4.4}
\contentsline {chapter}{\numberline {4}Uma Avalia\IeC {\c c}\IeC {\~a}o Preliminar}{48}{chapter.4}
\contentsline {section}{\numberline {4.1}MobiLine}{48}{section.4.1}
\contentsline {section}{\numberline {4.2}GREat Tour}{50}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}An\IeC {\'a}lise e Defini\IeC {\c c}\IeC {\~o}es}{50}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Analisando o Modelo de \textit {Features}}{51}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Analisando os Artefatos}{52}{subsection.4.2.3}
\contentsline {subsection}{\numberline {4.2.4}Modularizando Servi\IeC {\c c}os}{53}{subsection.4.2.4}
\contentsline {subsubsection}{MySQLServiceProvider}{54}{subsubsection*.30}
\contentsline {subsubsection}{LoginService}{56}{subsubsection*.31}
\contentsline {subsubsection}{LoginServiceProvider}{58}{subsubsection*.32}
\contentsline {subsection}{\numberline {4.2.5}Descrevendo Servi\IeC {\c c}os e Variabilidades}{62}{subsection.4.2.5}
\contentsline {section}{\numberline {4.3}CIn Tour}{65}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Analisando o Modelo de \textit {Features}}{65}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Descrevendo Servi\IeC {\c c}os e Variabilidades}{67}{subsection.4.3.2}
\contentsline {section}{\numberline {4.4}Analisando e Integrando a Aplica\IeC {\c c}\IeC {\~a}o Cliente}{68}{section.4.4}
\contentsline {section}{\numberline {4.5}Considera\IeC {\c c}\IeC {\~o}es Finais}{70}{section.4.5}
\contentsline {chapter}{\numberline {5}Conclus\IeC {\~a}o}{72}{chapter.5}
\contentsline {section}{\numberline {5.1}Contribui\IeC {\c c}\IeC {\~o}es}{72}{section.5.1}
\contentsline {section}{\numberline {5.2}Trabalhos Futuros}{73}{section.5.2}
\contentsline {chapter}{Refer\IeC {\^e}ncias}{74}{section.5.2}
\contentsline {chapter}{Ap\IeC {\^e}ndice}{81}{section*.34}
\contentsline {chapter}{\numberline {A}Componentes Descritores}{82}{appendix.A}
\contentsline {section}{\numberline {A.1}Descritor de Servi\IeC {\c c}os}{82}{section.A.1}
\contentsline {section}{\numberline {A.2}Descritor de Variabilidades}{83}{section.A.2}
\contentsline {section}{\numberline {A.3}Descritor de WSDL}{84}{section.A.3}
